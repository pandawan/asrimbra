package edu.tsp.asr.asrimbra;

import edu.tsp.asr.asrimbra.entities.Mail;
import edu.tsp.asr.asrimbra.entities.MailingList;
import edu.tsp.asr.asrimbra.entities.Role;
import edu.tsp.asr.asrimbra.exceptions.MailNotFoundException;
import edu.tsp.asr.asrimbra.exceptions.PropertyNotRecognizedException;
import edu.tsp.asr.asrimbra.exceptions.StorageException;
import edu.tsp.asr.asrimbra.helpers.SparkHelper;
import edu.tsp.asr.asrimbra.helpers.TokenHelper;
import edu.tsp.asr.asrimbra.repositories.api.MailRepository;
import edu.tsp.asr.asrimbra.repositories.api.MailingListRepository;
import edu.tsp.asr.asrimbra.repositories.api.UserRepository;
import edu.tsp.asr.asrimbra.repositories.jpa.MailJPARepository;
import edu.tsp.asr.asrimbra.repositories.jpa.MailingListJPARepository;
import edu.tsp.asr.asrimbra.repositories.remote.UserRemoteRepository;
import edu.tsp.asr.asrimbra.transformers.JsonTransformer;
import edu.tsp.asr.asrimbra.transformers.XMLTransformer;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import spark.ResponseTransformer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.port;
import static spark.Spark.post;

public class MailboxManager {
    public static void main(String[] a) {
        ResponseTransformer transformer;
        String TOKEN_COOKIE_NAME;
        Integer PORT_LISTENED;
        String HIBERNATE_CONFIG_FILE;
        String DIRECTORY_MANAGER_URL;
        String DIRECTORY_MANAGER_ACCOUNT;
        String DEFAULT_NEWSLETTER;

        // Read property file
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);

            switch (prop.getProperty("MAILBOX_MANAGER_TRANSFORMER")) {
                case "json":
                    transformer = new JsonTransformer();
                    break;
                case "xml":
                    transformer = new XMLTransformer();
                    break;
                default:
                    throw new PropertyNotRecognizedException();
            }

            TOKEN_COOKIE_NAME = prop.getProperty("MAILBOX_MANAGER_TOKEN_COOKIE_NAME");
            PORT_LISTENED = Integer.valueOf(prop.getProperty("MAILBOX_MANAGER_PORT_LISTENED"));
            HIBERNATE_CONFIG_FILE = prop.getProperty("MAILBOX_MANAGER_HIBERNATE_CONFIG_FILE");
            DIRECTORY_MANAGER_URL = prop.getProperty("DIRECTORY_MANAGER_URL");
            DIRECTORY_MANAGER_ACCOUNT = prop.getProperty("DIRECTORY_MANAGER_ACCOUNT");
            DEFAULT_NEWSLETTER = prop.getProperty("MAILBOX_MANAGER_DEFAULT_NEWSLETTER");
        } catch (IOException | PropertyNotRecognizedException ex) {
            ex.printStackTrace();
            System.exit(-1);
            return;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        SessionFactory factory;
        try {
            factory = new Configuration().configure(HIBERNATE_CONFIG_FILE).buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        // Repositories used by the applications
        UserRepository userRepository = new UserRemoteRepository(DIRECTORY_MANAGER_URL);
        MailRepository mailRepository = new MailJPARepository(factory);
        MailingListRepository mailingListRepository = new MailingListJPARepository(factory);
        try {
            mailingListRepository.add(new MailingList(DEFAULT_NEWSLETTER));
        } catch (StorageException e) {
            e.printStackTrace();
        }
        // Populate repository in order to facilitate tests
        try {
            mailRepository.add(
                    new Mail(
                            "guyomarc@tem-tsp.eu",
                            "atilalla@tem-tsp.eu",
                            "title",
                            "content"
                    )
            );
            mailRepository.add(
                    new Mail(
                            "atilalla@tem-tsp.eu",
                            "guyomarc@tem-tsp.eu",
                            "title2",
                            "content2"
                    )
            );
        } catch (StorageException e) {
            e.printStackTrace();
        }

        // Specifies the port listened by the MailboxManager
        port(PORT_LISTENED);

        //Allow Cross-origin resource sharing
        before((request, response) -> {
            response.header(
                    "Access-Control-Allow-Origin",
                    request.headers("Origin")
            );
            response.header(
                    "Access-Control-Allow-Credentials",
                    "true"
            );
        });

        post("/connect", (request, response) -> {
            SparkHelper.checkQueryParamsNullity(request, "mail", "password");

            Optional<Role> opt = userRepository.getRoleByCredentials(
                    request.queryParams("mail"),
                    request.queryParams("password")
            );

            if (opt.isPresent()) {
                response.cookie(TOKEN_COOKIE_NAME, TokenHelper.get(request.queryParams("mail")));
                return "";
            } else {
                halt(403, "Bad credentials :(");
                return "";
            }
        }, transformer);

        get("/disconnect", (request, response) -> {
            response.removeCookie(TOKEN_COOKIE_NAME);
            return "";
        }, transformer);

        before("/mailbox/*", (request, response) -> {
            String token = request.cookie(TOKEN_COOKIE_NAME);
            if (!TokenHelper.checkToken(token)) {
                halt(401, "You are not logged in :(");
            }
        });

        get("/mailbox/", (request, response) -> {
            String userMail = TokenHelper.extractUserName(request.cookie(TOKEN_COOKIE_NAME));
            return mailRepository.getByUserMail(userMail);
        }, transformer);


        get("/mailbox/:id", (request, response) -> {
            try {
                Integer id = Integer.parseInt(request.params(":id"));
                String userMail = TokenHelper.extractUserName(request.cookie(TOKEN_COOKIE_NAME));
                return mailRepository.getByUserMailAndId(userMail, id);
            } catch (MailNotFoundException e) {
                halt(404, "Mail not found");
                return "";
            } catch (NumberFormatException e) {
                halt(400, "id is not a number");
                return "";
            }
        }, transformer);

        delete("/mailbox/:id", (request, response) -> {
            Integer id = 0;
            try {
                id = Integer.parseInt(request.params(":id"));
                String userMail = TokenHelper.extractUserName(request.cookie("token"));
                mailRepository.removeByUserMailAndId(userMail, id);
                response.status(204);
            } catch (NumberFormatException e) {
                halt(400, "id is not a number");
            }
            response.status(202); // No garanty of success.
            return "";
        }, transformer);

        post("/mailbox/send", (request, response) -> {
            SparkHelper.checkQueryParamsNullity(request, "to", "title", "content");

            String from = TokenHelper.extractUserName(request.cookie("token"));
            String to = request.queryParams("to");
            String title = request.queryParams("title");
            String content = request.queryParams("content");
            Mail newMail;

            // Check if the mail is sent to a mailing list
            Optional<MailingList> maybeMailingList = mailingListRepository.getByAddress(to);
            if (maybeMailingList.isPresent()) {
                // if so, we forward it directly to all of its subscribers
                MailingList mailingList = maybeMailingList.get();
                for (String subscriberMail : mailingList.getSubscribersMails()) {
                    // variable "to" is the mailingList address
                    newMail = new Mail(to, subscriberMail, title, content);
                    mailRepository.add(newMail);
                }
                return mailingList.getSubscribersMails().size();
            } else {
                // else we consider that it is a normal mail
                newMail = new Mail(from, to, title, content);
                mailRepository.add(newMail);
                return newMail.getId();
            }
        }, transformer);

        before("/admin/*", (request, response) -> {
            SparkHelper.checkQueryParamsNullity(request, "token");

            String token = request.queryParams("token");
            if (!TokenHelper.checkToken(token) ||
                    !TokenHelper.extractUserName(token).equals(DIRECTORY_MANAGER_ACCOUNT)) {
                halt(401, "Nothing to do here :(");
            }
        });

        post("/admin/userWasAdded", ((request, response) -> {
            SparkHelper.checkQueryParamsNullity(request, "mail");
            mailingListRepository.addSubscriber(DEFAULT_NEWSLETTER, request.queryParams("mail"));
            return "";
        }));

        post("/admin/userWasRemoved", ((request, response) -> {
            SparkHelper.checkQueryParamsNullity(request, "mail");
            mailRepository.removeByUserMail(request.queryParams("mail"));
            return "";
        }));
    }

    public void init() {
        try {
            this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
